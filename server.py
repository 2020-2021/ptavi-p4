#!/usr/bin/python3
"""
Clase (y programa principal) para un servidor de eco en UDP simple
"""

import socketserver
import sys
import json
import time


class SIPRegisterHandler(socketserver.DatagramRequestHandler):
    """
    Echo server class
    """

    dic_data = {}
    dic_usuario = {}



    def handle(self):
        """
        handle method of the server class
        (all requests will be handled by this method)
        """
        self.json2registerd()
        for line in self.rfile:
            menssaje = line.decode('utf-8')

            if menssaje != '\r\n':
                mensaje = menssaje[0:-2].split(' ')
                uusuario = mensaje[1].split(':')

                if mensaje[0] != "REGISTER" and mensaje[0] != "Expires:":
                    print('Error404: REGISTER not found')

                if mensaje[0] == "Expires:":
                    temp_c = (time.time() + int(mensaje[1]))
                    temp_ex = time.strftime('%Y-%m-%d %H:%M:%S',
                                               time.tiempoloc(temp_c))
                    temp_ac = time.strftime('%Y-%m-%d %H:%M:%S',
                                               time.tiempoloc())
                    if int(mensaje[1]) == 0 or temp_ac >= temp_ex:
                        del self.dic_usuario[usuario]
                        self.register2json()
                    else:
                        self.dic_data['Address'] = self.client_address[0]
                        self.dic_data['Expires'] = temp_ex
                        self.dic_usuario[usuario] = self.dic_data
                        self.register2json()

                usuario = uusuario[-1]
                print(mensaje[0], mensaje[1])

        print(self.dic_usuario)
        self.wfile.write(b"SIP/2.0 200 OK\r\n\r\n")

   def register2json(self):
        with open("registered.json", "w") as ficherojson:
            json.dump(self.dic_usuario, ficherojson, indent=2)

    def json2registerd(self):
        try:
            with open("registered.json", 'r')as archivojson:
                self.dic_usuario = json.load(archivojson)
        except KeyError:
            pass


if __name__ == "__main__":
    # Listens at localhost ('') port 6001
    # and calls the EchoHandler class to manage the request
    Puert = int(sys.argv[1])
    
    serv = socketserver.UDPServer(('', Puert), SIPRegisterHandler)

    print("Lanzando servidor UDP de eco...")
    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        print("Finalizado servidor")
